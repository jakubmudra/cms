<?php
/**
 * Created by PhpStorm.
 * User: jakubmudra
 * Date: 10.02.18
 * Time: 12:37
 */




session_start();
define("CONTR_PATH",'controler/');

define("SYS_VER",'1');
define("SYS_UPD",'8.2.2018');

define("MODEL_PATH",'model/');
define('VIEW_PATH','view/');
define('MODULE_PATH','model/modules/');
define('UPLOAD_DIR','uploads/');
define('TEMPL_PRIP','.phtml');

/**
 *
 * Autoload function
 *
 * @param    class name
 * @return   included model or controler
 *
 */
function customAutoload($class)
{
    // EIs end of classname "Controler" ?
    if (preg_match('/Controler$/', $class))
        require(CONTR_PATH . $class . ".php");
    else if (preg_match('/Module$/', $class))
        require(MODULE_PATH . $class . ".php");
    else
        require(MODEL_PATH . $class . ".php");
}

// Register autoload
spl_autoload_register("customAutoload");


//Load from conf.ini
$ini_array = parse_ini_file("conf.ini");


// Připojení k databázi
Db::connect($ini_array['dbserver'],$ini_array['dbuser'],$ini_array['dbpass'],$ini_array['dbname']);


require(MODEL_PATH . "functions.php");


define("WEB_TITLE",get_web_name());

//Create router
$router = new RouterControler();

//Process URL to router
$router->process(array($_SERVER['REQUEST_URI']));


//Render current page
$router->render();

