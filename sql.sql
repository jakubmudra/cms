-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Počítač: wm131.wedos.net:3306
-- Vygenerováno: Ned 14. led 2018, 02:52
-- Verze serveru: 10.0.21-MariaDB
-- Verze PHP: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `d143516_pir`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `content` text COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=21 ;

--
-- Vypisuji data pro tabulku `articles`
--

INSERT INTO `articles` (`article_id`, `title`, `content`, `url`, `description`, `keywords`, `thumbnail`) VALUES
(17, 'Testovácí článek', 'asddas', 'testovaci-clanek', 'Tento článek je vytvořen pro testování nastavení a konfigurace tohoto systému. Lze ho smazat v administraci.', 'asdasd', 'uploads/2018-01/1515005826.png'),
(18, 'Testovácí článek', 'asasd', 'testovaci-clanek2', 'Tento článek je vytvořen pro testování nastavení a konfigurace tohoto systému. Lze ho smazat v administraci.', 'asdasd', 'uploads/2018-01/1515005987.png'),
(20, 'Lorem ipsum', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus faucibus molestie nisl. Aliquam erat volutpat. Duis pulvinar. Mauris metus. Integer imperdiet lectus quis justo. Proin in tellus sit amet nibh dignissim sagittis. Phasellus et lorem id felis nonummy placerat. Nunc tincidunt ante vitae massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Duis condimentum augue id magna semper rutrum. Fusce aliquam vestibulum ipsum. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam.</p>\r\n<p>In convallis. Integer imperdiet lectus quis justo. Vivamus luctus egestas leo. Etiam bibendum elit eget erat. Fusce tellus. Aenean vel massa quis mauris vehicula lacinia. Aenean id metus id velit ullamcorper pulvinar. Praesent vitae arcu tempor neque lacinia pretium. In dapibus augue non sapien. Mauris dictum facilisis augue. Aenean vel massa quis mauris vehicula lacinia. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede.</p>\r\n<p>Vestibulum fermentum tortor id mi. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Aenean vel massa quis mauris vehicula lacinia. Duis viverra diam non justo. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Donec iaculis gravida nulla. Aliquam ante. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Aliquam erat volutpat. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci.</p>\r\n<p>Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Etiam quis quam. Curabitur bibendum justo non orci. Fusce nibh. Etiam posuere lacus quis dolor. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Duis risus. Aenean id metus id velit ullamcorper pulvinar. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Aliquam erat volutpat. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Aliquam erat volutpat.</p>\r\n<p>Donec vitae arcu. Nunc dapibus tortor vel mi dapibus sollicitudin. In dapibus augue non sapien. Integer imperdiet lectus quis justo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Fusce nibh. Curabitur vitae diam non enim vestibulum interdum. Duis condimentum augue id magna semper rutrum. Quisque tincidunt scelerisque libero. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Etiam quis quam. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim.</p>', 'lorem-ipsum', 'Lorem ispum let amet', 'lorem ispum dolor set amet', 'uploads/2018-01/1515066763.jpg');

-- --------------------------------------------------------

--
-- Struktura tabulky `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_ID` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `IP` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `page` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `action` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=61 ;



-- --------------------------------------------------------

--
-- Struktura tabulky `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  `activate` int(11) NOT NULL,
  `slug` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  `verze` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `modules`
--

INSERT INTO `modules` (`id`, `name`, `description`, `activate`, `slug`, `verze`) VALUES
(1, 'Obecný modul', 'Obecný modul pro nádstavbu ostatními moduly. Tento modul nelze zakázat, aktivovat ani nic jiného', 0, 'module', '0.9 alfa');

-- --------------------------------------------------------

--
-- Struktura tabulky `option`
--

CREATE TABLE IF NOT EXISTS `option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(250) COLLATE utf8_czech_ci NOT NULL,
  `value` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=29 ;

--
-- Vypisuji data pro tabulku `option`
--

INSERT INTO `option` (`id`, `key`, `value`) VALUES
(1, 'web_name', 'Ivan Bartoš'),
(2, 'user_motto', 'Technologie, které máme nyní k dispozici, nám mohou úžasně pomáhat a ve finále všem lidem ulehčit život. Nesmí se ovšem stát nástrojem digitální totality.'),
(3, 'web_photo', 'uploads/2018-01/1515008855.jpg'),
(4, 'fb', 'https://www.facebook.com/ceska.piratska.strana/'),
(5, 'linkedin', 'https://cz.linkedin.com/in/ivan-bartos-6b0533b'),
(6, 'pirati', 'https://www.pirati.cz/'),
(17, 'img_url', 'uploads/2018-01/1515008855.jpg'),
(18, 'email', 'demo@demo.cz'),
(19, 'header_bg_color', '#343a40'),
(20, 'footer_bg_color', '#343a40'),
(21, 'header_text_color', '#ffffff'),
(22, 'footer_text_color', '#ffffff'),
(23, 'g_calendar', '&lt;iframe src=&quot;https://calendar.google.com/calendar/embed?height=600&amp;amp;wkst=1&amp;amp;bgcolor=%23FFFFFF&amp;amp;src=ndd91kcpm6fsorbq8o12j90r08%40group.calendar.google.com&amp;amp;color=%23691426&amp;amp;ctz=Europe%2FPrague&quot; style=&quot;border-width:0&quot; width=&quot;800&quot; height=&quot;600&quot; frameborder=&quot;0&quot; scrolling=&quot;no&quot;&gt;&lt;/iframe&gt;'),
(24, 'kancelar', 'Žitná, Praha'),
(25, 'twitter', 'https://twitter.com/PiratskaStrana?lang=cs'),
(26, 'omne', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris metus. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Maecenas sollicitudin. Nullam at arcu a est sollicitudin euismod. Integer vulputate sem a nibh rutrum consequat. Aliquam erat volutpat. Etiam egestas wisi a erat. Nullam eget nisl. Cras elementum. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Nulla est. Integer in sapien. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Mauris elementum mauris vitae tortor. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat.</p>\r\n<p>Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Donec iaculis gravida nulla. Integer vulputate sem a nibh rutrum consequat. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Curabitur bibendum justo non orci. Cras elementum. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Praesent id justo in neque elementum ultrices. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Aliquam id dolor. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Aliquam in lorem sit amet leo accumsan lacinia. Vestibulum fermentum tortor id mi.</p>\r\n<p>Phasellus et lorem id felis nonummy placerat. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Nunc dapibus tortor vel mi dapibus sollicitudin. Aliquam ante. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat. Aliquam ante. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Et harum quidem rerum facilis est et expedita distinctio. Nulla pulvinar eleifend sem. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Phasellus et lorem id felis nonummy placerat. Pellentesque pretium lectus id turpis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent in mauris eu tortor porttitor accumsan. Duis viverra diam non justo. Fusce wisi. Curabitur bibendum justo non orci.</p>\r\n<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Ut tempus purus at lorem. Fusce wisi. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Nam sed tellus id magna elementum tincidunt. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Aliquam in lorem sit amet leo accumsan lacinia. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Sed ac dolor sit amet purus malesuada congue. Aliquam erat volutpat. Duis risus.</p>\r\n<p>Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Aliquam erat volutpat. Nulla quis diam. Vivamus luctus egestas leo. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Proin mattis lacinia justo. Pellentesque arcu. Proin in tellus sit amet nibh dignissim sagittis. Cras elementum. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Nullam faucibus mi quis velit. Fusce consectetuer risus a nunc.</p>'),
(27, 'web_bg_color', '#ffffff'),
(28, 'web_text_color', '#000000');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `nick` (`nick`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=7 ;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`user_id`, `nick`, `password`, `email`, `admin`) VALUES
(5, 'demo', '6200c2c5ba8f8f3c4f615f52544d6e62ceb5206f95336fa84fe3c41e4524a72e', 'demo@demo.cz', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
