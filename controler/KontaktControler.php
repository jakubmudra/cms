<?php

class KontaktControler extends Controler
{
        public function process($param)
        {
                $this->header = array(
                        'title' => 'Kontaktní formulář',
                        'keywords' => 'kontakt, email, formulář',
                        'desc' => 'Kontaktní formulář našeho webu.'
                );


                if ($_POST)
                {
                        try
                        {
                                $mailSender = new mailSender();
                                $mailSender->sendWithAntispam($_POST['rok'], get_option("email")["value"], "Email z webu", $_POST['zprava'], $_POST['email']);
                                $this->addMessage('Email byl úspěšně odeslán.',"ok");
                                $this->redirect('kontakt');
                        }
                        catch (userException $chyba)
                        {
                                $this->addMessage($chyba->getMessage(),"error");
                        }
                }

                $this->view = 'contactForm';
    }
}