<?php


// Main controler
abstract class Controler
{

	// This array will be extracted to render
    protected $data = array();
	// Template name
    protected $view = "";
	// HTML page header
	protected $header = array('title' => '', 'keywords' => '', 'desc' => '');

	/**
 	*
	* Render actual view
	*
	* @param    nothing
	* @return      required view
	*
	*/


    public function render()
    {
        if ($this->view)
        {
            extract($this->sanitaze($this->data));
            extract($this->data, EXTR_PREFIX_ALL, "");
            require(VIEW_PATH . $this->view . TEMPL_PRIP);
        }
    }
	
	/**
	 *
	 * Redirect and close connection and end of this script
	 *
	 * @param    string url to redirect
	 * @return   nothing
	 *
	 */
	public function redirect($url)
	{
		header("Location: /$url");
		header("Connection: close");
        exit;
	}
	private function sanitaze($x = null)
	{
	        if (!isset($x))
	                return null;
	        elseif (is_string($x))
	                return htmlspecialchars($x, ENT_QUOTES);
	        elseif (is_array($x))
	        {
	                foreach($x as $k => $v)
	                {
	                        $x[$k] = $this->sanitaze($v);
	                }
	                return $x;
	        }
	        else
	                return $x;
	}

	public function addMessage($msg,$type)
	{
	        if (isset($_SESSION['msg']))
	                $_SESSION['msg'][] = [$msg,$type];
	        else
	                $_SESSION['msg'] = array(array($msg,$type));
	}

	public static function returnMessages()
	{
	        if (isset($_SESSION['msg']))
	        {
	                $msg = $_SESSION['msg'];
	                unset($_SESSION['msg']);
	                return $msg;
	        }
	        else
	                return array();
	}


	public function verifyUser($admin = false)
	{
	        $um = new userManager();
	        $user = $um->returnUser();
	        if (!$user || ($admin && !$user['admin']))
	        {
	                $this->addMessage('Nedostatečná oprávnění.','error');
	                $this->redirect('login');
	        }
	}
	// Hlavní metoda controlleru
    abstract function process($param);


}