<?php
class RegisterControler extends Controler
{
    public function process($param)
    {
                // Hlavička stránky
                $this->header['title'] = 'Registrace';
                if ($_POST)
                {
                        try
                        {
                                $um = new userManager();
                                $um->register($_POST['nick'], $_POST['password'], $_POST['password2'], $_POST['year']);
                                $um->login($_POST['nick'], $_POST['password']);
                                $this->addMessage('Byl jste úspěšně zaregistrován.','ok');
                                $this->redirect('admin');
                        }
                        catch (userException $chyba)
                        {
                                $this->addMessage($chyba->getMessage(),'error');
                        }
                }
                // Nastavení šablony
                $this->view = 'register';
    }
}