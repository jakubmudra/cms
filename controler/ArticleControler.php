<?php
class ArticleControler extends Controler
{
        public function process($param)
        {
                // Vytvoření instance modelu, který nám umožní pracovat s články
                $am = new ArticleManager();
                $um = new userManager();
                $user = $um->returnUser();
                $this->data['admin'] = $user && $user['admin'];

                // Je zadáno URL článku ke smazání
                if (!empty($param[1]) && $param[1] == 'remove')
                {
                        $this->verifyUser(true);
                        $am->deleteArticle($param[0]);
                        $this->addMessage('Článek byl úspěšně odstraněn','ok');
                        $this->redirect('article');
                }

                if(!empty($param[0]))
                {
                        // Získání článku podle URL
                        $article = $am->returnArticle($param[0]);
                        // Pokud nebyl článek s danou URL nalezen, přesměrujeme na ChybaKontroler
                        if (!$article)
                                $this->redirect('error');

                        // Hlavička stránky
                        $this->header = array(
                                'title' => $article['title'],
                                'keywords' => $article['keywords'],
                                'desc' => $article['description'],
                        );

                        // Naplnění proměnných pro šablonu
                        $this->data['article'] = $article;
                        $this->data['title'] = $article['title'];
                        $this->data['content'] = $article['content'];
                        

                        // Nastavení šablony
                        $this->view = 'article-single';
                }
                else
                {
                        $articles = $am->returnArticles();
                        $this->data['articles'] = $articles;
                        $this->view = 'article';
                }
        }




}