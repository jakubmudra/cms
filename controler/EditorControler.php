<?php
class EditorControler extends Controler
{
    public function process($param)
    {
                $this->verifyUser(true);
                // Hlavička stránky
                $this->header['title'] = 'Editor článků';
                // Vytvoření instance modelu
                $am = new ArticleManager();
                // Příprava prázdného článku
                $article = array(
                        'article_id' => '',
                        'title' => '',
                        'content' => '',
                        'url' => '',
                        'description' => '',
                        'keywords' => '',
                        'thumbnail' => '',
                );
                // Je odeslán formulář
                if ($_POST)
                {
                        // Získání článku z $_POST

                     
                        
                            $keys = array('title', 'content', 'url', 'description', 'keywords','thumbnail');
                            $article = array_intersect_key($_POST, array_flip($keys));

                            //Upload

                            $fm = new fileManager();
                            try {
                                if(empty($_FILES['thumbnail'])){
                                    $path = $_POST['img_url'];
                                }else{
                                    $path = $fm->upload($_FILES['thumbnail']);
                                }
                                $article['thumbnail'] = $path;

                                $article['url'] = generate_slug($_POST['title']);

                                $articles2 = $am->returnArticle($article['url']);

                                if(!empty($articles2)){
                                    $article['url'] .= "-2";
                                }




                               
                                  // Uložení článku do DB
                                $am->insertArticle($_POST['article_id'], $article);
                                $this->addMessage('Článek byl úspěšně uložen.','ok');
                                $this->redirect('../admin/blog/');
                            } catch (userException $error) {
                                $this->addMessage($error->getMessage(),'error');
                                // $this->redirect('editor');
                            }
                            
                          
                 
                             
                          
                       
                }
                // Je zadané URL článku k editaci
                else if (!empty($param[0]))
                {
                        $loadedArticle = $am->returnArticle($param[0]);
                        if ($loadedArticle)
                                $article = $loadedArticle;
                        else
                                $this->addMessage('Článek nebyl nalezen','error');
                }

                $this->data['article'] = $article;
                $this->view = 'article-editor';
    }
}