<?php
/**
* 
*/
class RouterControler extends Controler
{
	protected $controler;
	
	public function process($param)
	{
		$parsedURL = $this->parseURL($param[0]);
		$parsed2 = $parsedURL;
		if(empty($parsedURL[0])){
			$this->controler = new IndexControler;

		}
		else{

			$controlerClass = $this->camelCaps(array_shift($parsedURL)) . "Controler";

			if(file_exists(CONTR_PATH . $controlerClass . ".php"))
				$this->controler = new $controlerClass;
			else
				// var_dump($controlerClass);
				$this->redirect('error');
		}

		$this->controler->process($parsedURL);

		$this->data['title'] = join(" | ",[WEB_TITLE,$this->controler->header['title']]);
		$this->data['desc'] = $this->controler->header['desc'];
		$this->data['keywords'] = $this->controler->header['keywords'];
		if($parsed2[0] == "admin" OR $parsed2[0] == "editor"){

			$user_id = $_SESSION['user']['user_id'] ?? "No-loged";
			$page = json_encode($parsed2);
			$action = json_encode($_POST);
			log_activity($user_id,$page,$action);
			$this->view = 'admin-layout';
		}	
		else{
			$this->view = 'layout';
		}

		$this->data['msg'] = $this->returnMessages();
	}

	/**
 	*
	 * Parse URL to controller
	 *
	 * @param    string url
	 * @return    array of urls
	 *
	 */
	private function parseURL($url)
	{
		$parsed = parse_url($url);
		$parsed['path'] = ltrim($parsed['path'],"/");
		$parsed['path'] = trim($parsed['path']);
		$explodedURL = explode("/",$parsed['path']);

		return $explodedURL;

	}

	/**
	 *
	 * Convert an string to camelcaps
	 *
	 * @param    string string to convert
	 * @return      converted string
	 *
	 */
	private function camelCaps($string)
	{
		$return = str_replace("-", " ", $string);
		$return = ucwords($return);
		$return = str_replace(" ", "", $return);
		return $return;
	}
}