<?php
class LoginControler extends Controler
{
    public function process($param)
    {
                $um = new userManager();
                if ($um->returnUser())
                        $this->redirect('admin');
                // Hlavička stránky
                $this->header['title'] = 'Přihlášení';
                if ($_POST)
                {
                	if(isset($_POST['nick']) && isset($_POST['password']))
                     {   try
                                             {
                                                     $um->login($_POST['nick'], $_POST['password']);
                                                     $this->addMessage('Byl jste úspěšně přihlášen.','ok');
                                                     $this->redirect('admin');
                                             }
                                             catch (userException $chyba)
                                             {
                                                     $this->addMessage($chyba->getMessage(),'error');
                                             }
                     }else{
                     	$this->addMessage("Musíte vyplnit uživatelské jméno i heslo!",'error');
                     }
                }
                // Nastavení šablony
                $this->view = 'login';
    }
}
