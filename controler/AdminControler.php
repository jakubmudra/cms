<?php
class AdminControler extends Controler
{
    public function process($param)
    {
         
        $um = new userManager();
        $this->verifyUser();
        $user = $um->returnUser();
        $this->data['nick'] = $user['nick'];
        //Save data from form to social option
        if(isset($_POST['save-social']))
        {

            if(!isset($_POST['show-fb-widget'])){
                edit_option("show-fb-widget","false");
            }
            if(!isset($_POST['show-twitter-widget'])){
                edit_option("show-twitter-widget","false");
            }

            //For each
            foreach ($_POST as $key => $value) {



                //If key != savebtn
                if($key != "save-social"):

                    edit_option($key,$value);


                endif;

                //endif
            }

            //Add verify message to user
            $this->addMessage("Nastavení bylo úspěšně uloženo","ok");
        }
        //Save data from profile to options
        if(isset($_POST['save-profil']))
        {

             //For each
            foreach ($_POST as $key => $value) {
                //If key != savebtn
                if($key != "save-profil"):

                        edit_option($key,$value);

                endif;
                //endif
            }

            if(!empty($_FILES['web_photo']['name'])){
                $fm = new fileManager();
                $path = $fm->upload($_FILES['web_photo']);
                edit_option('web_photo',$path);
            }


        }


               
                // Hlavička stránky
                $this->header['title'] = 'Přihlášení';
                // Získání dat o přihlášeném uživateli
                

                //If user want to logout
                if (!empty($param[0]) && $param[0] == 'logout')
                {
                        $um->logout();
                        $this->redirect('login');
                }

                //Return user info and set it to variables
               
                $this->data['admin'] = $user['admin'];
                //Set template - if is empty, load admin 
                if(empty($param[0])){
                 $this->view = 'admin';
                 //else if is blog
                }elseif($param[0] == "blog"){
                    //load article manager
                    $am = new ArticleManager;
                    $articles = $am->returnArticles();

                    //Set data to template and set view
                    $this->data['articles'] = $articles;
                    $this->view = 'article-admin';

                    //if is article to remove
                    if(isset($param[2]) && $param[2] == "remove"){
                        //Delete article
                        $am->deleteArticle($param[1]);
                        //Remove article
                        $this->addMessage('Článek byl úspěšně odstraněn','ok');
                        $this->redirect('admin/blog/');
                    }
                //if is social - open view
                }elseif($param[0] == "social"){

                   $this->view = 'admin-social';

                } elseif($param[0] == "profil"){

                   $this->view = 'admin-profil';

                }elseif($param[0] == "kontaktni-formular"){

                   $this->view = 'modules/kontaktni-formular';
                }elseif($param[0] == "design"){

                   $this->view = 'design';


                   if(isset($_POST['save-design'])){
                    foreach ($_POST as $key => $value) {
                        if($key != "save-design"){
                             if(get_option($key)){
                                    edit_option($key,$value);
                                }
                                else {
                                    insert_option($key,$value);
                                }
                        }
                       

                    }
                    $this->addMessage("Nastavení barev bylo úspěšně uloženo","ok");
                   }
                }
                elseif($param[0] == "moduly"){
                    $mm = new ModuleManager();
                   

                    if(isset($param[1]) && in_array($param[1], $mm->moduleList())){
                        $module = new $param[1];
                        $data = $module->returnInfo();


                        
                        $this->view = 'modules/'.$data['slug'];

                       

                    }else{
                     $modules = $mm->getModules();
                    $this->data['modules'] = $modules;
                   $this->view = 'modules/index';
                   }
                }elseif($param[0] == "uzivatele"){
                    $um = new UserManager();

                    if(isset($param[1]) && $param[1] == "odstranit" && isset($param[2])){
                        
                        
                        if($param[2] == $_SESSION['user']['user_id']){
                           $this->addMessage("Nelze odstranit vlastní účet!","error");
                        }else{
                            $um->remove($param[2]);
                            $this->addMessage("Uživatel byl úspěšně odstraněn","ok");
                        }
                    }elseif(isset($param[1]) && $param[1] == "pridat"){
                        if(isset($_POST['save-user'])){
                           
                            try {
                                 $um->register($_POST['nick'],$_POST['password'],$_POST['password2'],$_POST['email']);
                                $this->redirect('../admin/uzivatele');
                            } catch (UserException $e) {
                                $this->addMessage($e->getMessage(),"error");
                                $this->data['user'] = $_POST;
                                $this->view = 'register';
                            }
                        }else{
                            $this->view = 'register';
                        }  
                    }elseif(isset($param[1]) && $param[1] == "upravit"){
                       
                        if(isset($_POST['save-user'])){
                            $um->update($_POST['nick'],$_POST['password'],$_POST['password2'],$param[2],$_POST['email']);
                        }




                        if(isset($param[2])){
                            $user = $um->getUser($param[2]);
                            $this->data['user'] = $user;
                        }
                        $this->view = "register";
                    }else{
                        $users = $um->getAllusers();
                        $this->data['uzivatele'] = $users;
                        $this->view = 'uzivatele';
                    }



                    
                   
                }
                    
                    // $this->controler = new EditorControler;
                    // $this->controler->process(array_shift($param));
                    // $this->controler->render();
                
    }
}