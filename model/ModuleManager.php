<?php

// 
class ModuleManager
{

	/**
	 *
	 * Return modules from db
	 * @return Allrows
	 */
	public function returnModules()
        {
                return Db::allRows('
                        SELECT `id`, `name`, `description`, `activate`, `verze`
                        FROM `modules`
                        ORDER BY `id` DESC
                ');
        }


	/**
	 * @return array
	 */
	public function moduleList(){
                $modules = $this->getModules();
                return array_column($modules, 'slug');
        }

	/**
	 * @return array
	 */
	public function getModules()
        {
                $files = scandir(MODULE_PATH);
                $all = [];
                foreach ($files as $file) {
                        if(strlen($file) > 5)
                        {
                                $fileName = str_replace(".php", "", $file);
                                $manager = new $fileName;
                                $data = $manager->returnInfo();
                                $all[] = $data;
                        }
                
                }
                return $all;
        }

	/**
	 * @param $slug
	 *
	 * @return one
	 */
	public function returnModule($slug)
        {
                return Db::oneRow('
                        SELECT `id`, `name`, `description`, `activate`, `verze`
                        FROM `modules`
                        WHERE `slug` = ?
                ', array($slug));
        }

	/**
	 * @param $slug
	 *
	 * @return bool
	 */
	public function getModuleID($slug)
        {
                $id = $this->returnModule($slug);
                if($id['id']){
                        return $id['id'];
                }else return False;
        }


}