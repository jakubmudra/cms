<?php

/**
* 
*/
class db
{
	
	private static $conn;

	private static $conf = array(
	        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
	        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
	        PDO::ATTR_EMULATE_PREPARES => false,
	);
	/**
	 *
	 * Connect to DB
	 *
	 * @param    string host
	 * @param 	 string user
	 * @param 	 string pass
	 * @param    string dbname
	 *
	 */
	public static function connect($host, $user, $pass, $dbname)
	{
	        if (!isset(self::$conn))
	        {
	                self::$conn = @new PDO(
	                        "mysql:host=$host;dbname=$dbname",
	                        $user,
	                        $pass,
	                        self::$conf
	                );
	        }
	}

	/**
	 *
	 * Return OneRow
	 *
	 * @param    query 
	 * @param    array param
	 * @return      one row
	 *
	 */
	public static function oneRow($query, $param = array())
	{
	        $return = self::$conn->prepare($query);
	        $return->execute($param);
	        return $return->fetch();
	}
	/**
	 *
	 * Return AllRows
	 *
	 * @param    query 
	 * @param    array param
	 * @return      Allrows
	 *
	 */
	public static function allRows($query, $param = array())
	{
	        $return = self::$conn->prepare($query);
	        $return->execute($param);
	        return $return->fetchAll();
	}
	/**
	 *
	 * Return One Collumn
	 *
	 * @param    query 
	 * @param    array param
	 * @return      one collumn
	 *
	 */
	public static function oneCol($query, $param = array())
	{
	        $return = self::oneRow($query, $param);
	        return $return[0];
	}
	/**
	 *
	 * Return affected row
	 *
	 * @param    query 
	 * @param    array param
	 * @return      rowCount
	 *
	 */
	public static function query($query, $param = array())
	{
	        $return = self::$conn->prepare($query);
	        $return->execute($param);
	        return $return->rowCount();
	}


	public static function insert($table, $param = array())
	{
	        return self::query("INSERT INTO `$table` (`".
	                implode('`, `', array_keys($param)).
	                "`) VALUES (".str_repeat('?,', sizeOf($param)-1)."?)",
	                        array_values($param));
	}

	public static function edit($table, $values = array(), $condition, $param = array())
	{
	        return self::query("UPDATE `$table` SET `".
	                implode('` = ?, `', array_keys($values)).
	                "` = ? " . $condition,
	                array_merge(array_values($values), $param));
	}

	public static function getLastId()
	{
	        return self::$conn->lastInsertId();
	}
}