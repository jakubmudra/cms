<?php


/**
* 
* 	DEFAULT MODULE CLASS
*		NEW MODULES ONLY BY EXTEND THIS CLASS
*
*
*/
class Module
{


	public $data = array(
						'name' => "Obecný modul",
						'verze' => "0.9 alfa",
						'description' => "Obecný modul pro nádstavbu ostatními moduly. Tento modul nelze zakázat, aktivovat ani nic jiného",
						'slug' => 'module');

	private $keys = array('name', 'description', 'verze', 'slug');


	/**
	 * regster class
	 */
	public function register()
	{
        $saved = array_intersect_key($this->data, array_flip($this->keys));
        Db::insert('modules',$saved);
	}

	/**
	 * @return array
	 */
	public function returnInfo()
	{
		return array_intersect_key($this->data, array_flip($this->keys));
	}
}