<?php
/**
* 
*/
class kontaktniFormularModule extends Module
{
	public $data = array(
						'name' => "Modul pro kontaktní formulář",
						'verze' => "0.5",
						'description' => "Modul pro zobrazování kontaktního formuláře a jeho odesílání",
						'slug' => 'kontaktniFormularModule');


	private $keys = array('name', 'description', 'verze', 'slug');

	public function returnInfo()
	{
		return array_intersect_key($this->data, array_flip($this->keys));
	}

}