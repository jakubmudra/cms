<?php
/**
* 
*/
class logovaniModule extends Module
{
	public $data = array(
						'name' => "Modul pro logování aktivity",
						'verze' => "0.2",
						'description' => "Modul pro logování aktivity uživatele a následné dohledání neobvyklých aktivit.",
						'slug' => 'logovaniModule');


	private $keys = array('name', 'description', 'verze', 'slug');

	/**
	 * @return array
	 */
	public function returnInfo()
	{
		return array_intersect_key($this->data, array_flip($this->keys));
	}


	/**
	 *
	 */
	public function hello(){
		echo "hello";
	}

	/**
	 * @param $user_id
	 *
	 * @return Allrows
	 */
	public function get_activities($user_id)
	{
	 //
                return Db::allRows('
                        SELECT `id`, `user_ID`, `IP`, `page`, `datetime`
                        FROM `log`
                        WHERE `user_ID` = ?
                ', array($user_id));
       
	}

}