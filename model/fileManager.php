<?php

/**
* 
*/
class fileManager
{

	public $max_file_upload = 50000;

	public $allowed_formats = array('jpg','png','gif','jpeg');


	/**
	 * Upload File from form
	 * @param $file
	 *
	 * @return string
	 * @throws userException
	 */
	function upload($file)
	{
		$current_format = date("Y-m");

		//If folder to upload with current date not exist, create it
		if(!file_exists(UPLOAD_DIR . $current_format )){
			mkdir(UPLOAD_DIR . $current_format, 0777, true);
		}

		$state = 0;

		$temp = explode(".", $file["name"]);

		$newfilename = round(microtime(true)) . '.' . end($temp);
		
		$targer_file = UPLOAD_DIR . $current_format . "/" . $newfilename;

		

		$imageFileType = strtolower(pathinfo($targer_file,PATHINFO_EXTENSION));

		//Chech if is image
		$check = getimagesize($file['tmp_name']);

		if($check !== false){
			$state = 1;
		}
		else
		{
		   throw new userException('Špatný formát obrázku.');
		   $state = 0;
		}


		//Check, if file allredy exists
		if(file_exists($targer_file)){
		   throw new userException('Omlouváme se, ale takovýto obrázek už zde existuje, přejmenujte ho prosím.');
		   $state = 0;
		}

		//Check file size
		if($file['size'] > 5000000)
		{
			throw new userException("Omlouváme se, ale nahrávaný soubor je moc velký");
			$state = 0;
		}

		if($state == 0)
		{
			throw new userException("Omlouváme se, ale Váš obrazek nemohl být nahrán.");
		}else{
			if(move_uploaded_file($file['tmp_name'], $targer_file)){
				return $targer_file;
			}else{
				throw new userException("Je nám líto, ale požadovaný obrázek se nepodařio nahrát");
			}
		}


	}

}