<?php

class UserManager
{


	/**
	 * @param $password
	 *
	 * @return string
	 */
	public function hash_password($password)
        {
                $salt = 'fd16sdfd2ew#$%';
                return hash('sha256', $password . $salt);
        }


	/**
	 * Register user
	 * @param $nick
	 * @param $password
	 * @param $password2
	 * @param $email
	 *
	 * @return bool
	 * @throws userException
	 */
	public function register($nick, $password, $password2,$email)
        {
                if ($password != $password2)
                        throw new userException('Hesla nesouhlasí.');
                $user = array(
                        'nick' => $nick,
                        'password' => $this->hash_password($password),
                        'email' => $email,
                );
                try
                {
                        Db::insert('users', $user);
                        return true;
                }
                catch (PDOException $chyba)
                {
                        throw new userException('Uživatel s tímto jménem je již zaregistrovaný.');
                        return false;
                }
        }

	/**
	 * @return Allrows
	 */
	public function getAllUsers()
        {
          
                return Db::allRows('
                        SELECT `user_id`, `nick`, `email`, `admin`
                        FROM `users`
                        ORDER BY `user_id` ASC
                ');
        
        }

	/**
	 * @param $user_id
	 *
	 * @return one
	 */
	public function getUser($user_id)
        {
                return Db::oneRow('
                        SELECT `user_id`, `nick`, `email`
                        FROM `users`
                        WHERE `user_id` = ?
                ', array($user_id));
        }




	/**
	 * Login user
	 * @param $nick
	 * @param $password
	 *
	 * @throws userException
	 */
	public function login($nick, $password)
        {
                $user = Db::oneRow('
                        SELECT user_id, nick, admin
                        FROM users
                        WHERE nick = ? AND password = ?
                ', array($nick, $this->hash_password($password)));
                if (!$user)
                        throw new userException('Neplatné jméno nebo heslo.');
                $_SESSION['user'] = $user;
        }

	/**
	 * remove user
	 *
	 * @param $user_id
	 *
	 * @return rowCount
	 */
	public function remove($user_id)
        {
             return Db::query('
                    DELETE FROM users
                    WHERE user_id = ?
            ', array($user_id));
        }



	/**
	 *Logout user
	 */
	public function logout()
        {
            unset($_SESSION['user']);
        }


	/**
	 * @return null
	 */
	public function returnUser()
        {
                if (isset($_SESSION['user']))
                        return $_SESSION['user'];
                return null;
        }


	/**
	 * @param $nick
	 * @param $password
	 * @param $password2
	 * @param $id
	 * @param $email
	 *
	 * @throws userException
	 */
	public function update($nick, $password, $password2,$id,$email)
        {
                if (isset($password) && $password != $password2)
                        throw new userException('Hesla nesouhlasí.');
                $user = array(
                        'nick' => $nick,
                        'email' => $email
                );
                if(isset($password)){
                        $user['password'] = $this->hash_password($password);
                }
                try
                {
                        Db::edit('users',$user,'WHERE user_id = ?', array($id));
                }
                catch (PDOException $chyba)
                {
                        throw new userException('Uživatel s tímto jménem je již zaregistrovaný.');
                }
        }

}