<?php
class mailSender
{


	/**
	 * Send mail
	 *
	 * @param $to
	 * @param $subject
	 * @param $message
	 * @param $from
	 *
	 * @throws userException
	 */
	public function send($to, $subject, $message, $from)
        {
                $header = "From: " . $from;
                $header .= "\nMIME-Version: 1.0\n";
                $header .= "Content-Type: text/html; charset=\"utf-8\"\n";
                if (!mb_send_mail($to, $subject, $message, $header))
                        throw new userException('Email se nepodařilo odeslat.');
        }

	/**
	 *
	 * Check antispam
	 * @param $rok
	 * @param $to
	 * @param $subject
	 * @param $message
	 * @param $header
	 *
	 * @throws userException
	 */
	public function sendWithAntispam($rok, $to, $subject, $message, $header)
        {
                if ($rok != date("Y"))
                        throw new userException('Chybně vyplněný antispam.');
                if(empty($message))
                		throw new userException("Musíte vyplnit text zrpávy.");
                		
                $this->send($to, $subject, $message, $header);
        }

}