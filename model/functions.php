<?php
/**
 * Get option by key
 * @param $key
 *
 * @return one
 */
function get_option($key)
	{
		return Db::oneRow( '
	                    SELECT `id`, `key`, `value`
	                    FROM `option`
	                    WHERE `key` = ?
	            ', array( $key ) );
	}

/**
 * Print option
 * @param $key
 */
function print_option($key)
	{
	    echo get_option($key)['value'];
	}

/**
 * Edut option - if exist, else it create it
 * @param $key
 * @param $value
 */
function edit_option($key, $value){
    if(get_option($key)){
        Db::query("UPDATE `option` SET `value`='".sanitaze($value)."' WHERE `key`='".sanitaze($key)."'");
    }else{
        insert_option($key,$value);
    }

}


/**
 * Insert new option
 * @param $key
 * @param $value
 *
 * @return rowCount
 */
function insert_option($key,$value)
	{
	    return Db::insert('option',array("key" => $key, "value" => $value));
	}

/**
 * Get web name
 * @return mixed
 */
function get_web_name()
	{
		return get_option('web_name')['value'];
	}

/**
 * Get web motto
 * @return mixed
 */
function get_motto()
	{
		return get_option('user_motto')['value'];
	}


/**
 *
 *  Get social link by name
 * @param $name
 *
 * @return mixed
 */
function get_social($name)
	{
		return get_option($name)['value'];
	}


/**
 * Get home url
 *
 * @param bool $atRoot
 * @param bool $atCore
 * @param bool $parse
 *
 * @return string
 */
function get_home_url($atRoot=FALSE, $atCore=FALSE, $parse=FALSE)
	{
		$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

		return $root;
	}


/**
 * Get main web photo
 *
 * @return mixed
 */
function get_photo()
	{
		return get_option('web_photo')['value'];
	}


/**
 * Sanitaze Variable/array
 *
 * @param null $x
 *
 * @return array|null|string
 */
function sanitaze($x = null)
	{
	        if (!isset($x))
	                return null;
	        elseif (is_string($x))
	                return htmlspecialchars($x, ENT_QUOTES);
	        elseif (is_array($x))
	        {
	                foreach($x as $k => $v)
	                {
	                        $x[$k] = $this->sanitaze($v);
	                }
	                return $x;
	        }
	        else
	                return $x;
	}


/**
 * Generate slug from title
 * @param $title
 *
 * @return null|string|string[]
 */
function generate_slug($title)
	{
	    $url = $title;
	    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
	    $url = trim($url, "-");
	    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
	    $url = strtolower($url);
	    $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
	    return $url;
	}


/**
 * Log user activity
 * @param $user_id
 * @param $page
 * @param $action
 */
function log_activity($user_id,$page,$action)
	{
	    $ip = $_SERVER['REMOTE_ADDR'];
	    $time = time();
	    $data = ["user_ID" => $user_id,
	             "IP" => $ip,
	             "page" => $page,
	             "action" => $action];


	    $last = get_last_activity($user_id)[0];


	    if(is_array($last)){

	        $now = strtotime(date('Y-m-d G:i:s'));
	        $target = strtotime($last['datetime']);

	        $diff = $now - $target;

	        if ( $diff < 900 && $last['page'] == $page && $IP == $last['IP']) {
	           echo "<script>console.log('Do not loging this activity, ip and page is same, time is less than 15 min from last activity');</script>";
	        }else {
	            Db::insert('log', $data);
	        }
	    }else {
	            Db::insert('log', $data);
	        }




	}


/**
 * Get last user activity
 * @param $user_id
 *
 * @return Allrows
 */
function get_last_activity($user_id)
	{
	    return Db::allRows('
	                        SELECT `id`, `user_ID`, `page`, `datetime`
	                        FROM `log`
	                        WHERE `user_ID` = ?
	                        ORDER BY `id` DESC LIMIT 1
	        ', array($user_id));
	}


/**
 * Is option selected
 * @param $key
 */
function selected($key){
    $option = get_option($key)["value"];
    if($option == "true"){
        echo "checked";
    }
}