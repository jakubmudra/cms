<?php

// 
class ArticleManager
{


	/**
	 * Return One article by url
	 * @param $url
	 *
	 * @return one
	 */
	public function returnArticle($url)
    {
            return Db::oneRow('
                    SELECT `article_id`, `title`, `content`, `url`, `description`, `keywords`, `thumbnail`
                    FROM `articles`
                    WHERE `url` = ?
            ', array($url));
    }


	/**
	 * Return all articles
	 * @return Allrows
	 */
	public function returnArticles()
    {
            return Db::allRows('
                    SELECT `article_id`, `title`, `content`, `url`, `description`, `keywords`, `thumbnail`
                    FROM `articles`
                    ORDER BY `article_id` DESC
            ');
    }


	/**
	 * Insert article to db
	 * @param $id article id for editing
	 * @param $article article object
	 */
	public function insertArticle($id, $article)
	{
		if (!$id)

			Db::insert('articles', $article);
		else
			Db::edit('articles', $article, 'WHERE article_id = ?', array($id));
	}


	/**
	 * Delete article by url
	 * @param $url
	 */
	public function deleteArticle($url)
    {
            Db::query('
                    DELETE FROM articles
                    WHERE url = ?
            ', array($url));
    }

}