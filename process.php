<?php
/**
 * Created by PhpStorm.
 * User: jakubmudra
 * Date: 16.01.18
 * Time: 19:53
 */

echo "Startuji instalaci";

$data = $_POST;

$return = ["dbuser" => $_POST["dbuser"],"dbpass" => $_POST["dbpass"],"dbserver" => $_POST["dbserver"],"dbname" => $_POST["dbname"]];

echo "Vkládám údaje o db do konfiguračního souboru.";
write_php_ini($return,"conf.ini");


// Connect to MySQL server
$link = mysqli_connect($return['dbserver'],$return['dbuser'],$return['dbpass'],$return['dbname']);

$filename = "sql.sql";

// Temporary variable, used to store current query
$templine = '';
// Read in entire file
$lines = file($filename);
// Loop through each line
foreach ($lines as $line)
{
// Skip it if it's a comment
    if (substr($line, 0, 2) == '--' || $line == '')
        continue;

// Add this line to the current segment
    $templine .= $line;
// If it has a semicolon at the end, it's the end of the query
    if (substr(trim($line), -1, 1) == ';')
    {
        // Perform the query
        mysqli_query($link,$templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error($link) . '<br /><br />');
        // Reset temp variable to empty
        $templine = '';
    }
}

$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';

echo "Testovací data byla úspěšně importována.";


echo "<hr/>";
echo  "<p>Instalace byla úspěšně dokončena. Pro vstup do administrace přejděte na ".$root."/admin/ a zadejte přihlašovací údaje <b>demo</b>, heslo <b>demo</b>.Ihned po přihlášení je potřeba tyto údaje změnit!";

unlink("instal.php");


/**
 * Write to ini
 * @param $array
 * @param $file
 */
function write_php_ini($array, $file)
{
    $res = array();
    foreach($array as $key => $val)
    {
        if(is_array($val))
        {
            $res[] = "[$key]";
            foreach($val as $skey => $sval) $res[] = "$skey = ".(is_numeric($sval) ? $sval : '"'.$sval.'"');
        }
        else $res[] = "$key = ".(is_numeric($val) ? $val : '"'.$val.'"');
    }
    safefilerewrite($file, implode("\r\n", $res));
}

/**
 *
 * Sefely write file
 * @param $fileName
 * @param $dataToSave
 */
function safefilerewrite($fileName, $dataToSave)
{
	if ($fp = fopen($fileName, 'w'))
		{
		    $startTime = microtime(TRUE);
		    do
    {
    	        $canWrite = flock($fp, LOCK_EX);
		        // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
		        if(!$canWrite) usleep(round(rand(0, 100)*1000));
		    } while ((!$canWrite)and((microtime(TRUE)-$startTime) < 5));

		    //file was locked so now we can store information
		    if ($canWrite)
		    {
		    	fwrite($fp, $dataToSave);
		        flock($fp, LOCK_UN);
		    }
		    fclose($fp);
		}

}