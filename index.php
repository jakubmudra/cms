<?php

// Set internal encoding
// error_reporting(0);
mb_internal_encoding("UTF-8");

error_reporting(0);


// Set safety function to stop php and dont show errors
register_shutdown_function('ShutDown');

//Include main loading file
include("start.php");


/**
 * Catch error and process it
 * @param $errno
 * @param $errstr
 * @param string $errfile
 * @param string $errline
 */
function catchError($errno, $errstr, $errfile = '', $errline = ''){
    include("error.html");
    exit();
}


/**
 * ShutDown function
 */
function ShutDown(){
    $lasterror = error_get_last();
    if(in_array($lasterror['type'],Array( E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR, E_CORE_WARNING, E_COMPILE_WARNING, E_PARSE))){
        catchError($lasterror['type'],$lasterror['message'],$lasterror['file'],$lasterror['line']);
    }
}
